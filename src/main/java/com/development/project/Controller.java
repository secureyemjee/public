package com.development.project;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@RequestMapping("yemjee")
	public String sayHello() {
		return ("yemjee says hello to java world");
	}
}
